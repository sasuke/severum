package io.github.sasuked.severum.world.storage;

import io.github.sasuked.severum.api.events.PlayerDataLoadEvent;
import io.github.sasuked.severum.api.events.PlayerDataSaveEvent;
import net.minecraft.server.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.plugin.PluginManager;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.UUID;

import static net.minecraft.server.NBTCompressedStreamTools.a;

/**
 * @author Luiz Carlos Mourão
 */
public class PlayerDataStorage implements IPlayerFileData {

  private static final boolean DISCARD_PLAYER_DATA = Bukkit.getSeverumConfig().isDiscardPlayerData();

  private static final Logger LOGGER = LogManager.getLogger();
  private static final String[] EMPTY_STRING_ARRAY = new String[0];

  private final File dataDir;
  private final File statsDir;

  public PlayerDataStorage(File baseDir, boolean flag) {
    if (DISCARD_PLAYER_DATA) {
      this.dataDir = null;
      this.statsDir = null;
      return;
    }

    this.dataDir = new File(baseDir, "playerdata");
    this.statsDir = new File(baseDir, "stats");

    this.dataDir.mkdirs();
    this.statsDir.mkdirs();
  }

  @Override
  public void save(EntityHuman entityHuman) {
    if (DISCARD_PLAYER_DATA) return;

    PluginManager manager = entityHuman.getWorld().getServer().getPluginManager();

    PlayerDataSaveEvent event = new PlayerDataSaveEvent(((CraftPlayer) entityHuman.getBukkitEntity()));
    manager.callEvent(event);


    if (event.isCancelled()) {
      return;
    }

    try {
      NBTTagCompound compound = new NBTTagCompound();
      entityHuman.e(compound);
      String uid = entityHuman.getUniqueID().toString();

      a(compound, Files.newOutputStream(this.dataDir.toPath().resolve(uid + ".dat")));
    } catch (Exception exception) {
      LOGGER.warn("failed to save player data for {}", entityHuman.getName());
    }
  }

  @Override
  public NBTTagCompound load(EntityHuman entityHuman) {
    if (DISCARD_PLAYER_DATA) return null;

    PluginManager manager = entityHuman.getWorld().getServer().getPluginManager();

    final PlayerDataLoadEvent event = new PlayerDataLoadEvent(((CraftPlayer) entityHuman.getBukkitEntity()));
    manager.callEvent(event);

    if (event.isCancelled()) {
      return null;
    }


    NBTTagCompound compound = null;

    try {
      File file = new File(this.dataDir, entityHuman.getUniqueID().toString() + ".dat");
      // Spigot Start
      boolean usingWrongFile = false;
      if (Bukkit.getOnlineMode() && !file.exists()) {
        file = new File(this.dataDir, UUID.nameUUIDFromBytes(("OfflinePlayer:" + entityHuman.getName()).getBytes(StandardCharsets.UTF_8)) + ".dat");
        if (file.exists()) {
          usingWrongFile = true;
          LOGGER.warn("falling back to offline mode UUID for player {}", entityHuman.getName());
        }
      }
      // Spigot End

      if (file.exists() && file.isFile()) {
        compound = NBTCompressedStreamTools.a(new FileInputStream(file));
      }
      // Spigot Start
      if (usingWrongFile) {
        file.renameTo(new File(file.getPath() + ".offline-read"));
      }
      // Spigot End
    } catch (Exception e) {
      LOGGER.warn("failed to load player data for {}", entityHuman.getName(), e);
    }

    if (compound != null) {
      // CraftBukkit start
      if (entityHuman instanceof EntityPlayer) {
        CraftPlayer player = (CraftPlayer) entityHuman.getBukkitEntity();
        // Only update first played if it is older than the one we have
        long modified = new File(this.dataDir, entityHuman.getUniqueID().toString() + ".dat").lastModified();
        if (modified < player.getFirstPlayed()) {
          player.setFirstPlayed(modified);
        }
      }
      // CraftBukkit end

      entityHuman.f(compound);
    }

    return compound;
  }

  @Override
  public String[] getSeenPlayers() {
    String[] fileList = this.dataDir.list();
    if (fileList == null) return EMPTY_STRING_ARRAY;

    for (int i = 0; i < fileList.length; ++i) {
      if (fileList[i].endsWith(".dat")) {
        fileList[i] = fileList[i].substring(0, fileList[i].length() - 4);
      }
    }

    return fileList;
  }

  @Override
  public NBTTagCompound getPlayerData(String name) {
    if (DISCARD_PLAYER_DATA) return null;

    try {
      File file = new File(this.dataDir, name + ".dat");

      if (file.exists()) {
        return NBTCompressedStreamTools.a(new FileInputStream(file));
      }
    } catch (Exception exception) {
      LOGGER.warn("Failed to load player data for " + name);
    }

    return null;
  }

  @Override
  public ServerStatisticManager createStatisticManager(EntityHuman entityHuman) {
    if (DISCARD_PLAYER_DATA) return new ServerStatisticManager(MinecraftServer.getServer(), null);

    File file = new File(this.statsDir, "stats");
    File file1 = new File(file, entityHuman.getUniqueID().toString() + ".json");

    if (!file1.exists()) {
      File file2 = new File(file, entityHuman.getName() + ".json");

      if (file2.exists() && file2.isFile()) {
        file2.renameTo(file1);
      }
    }

    return new ServerStatisticManager(MinecraftServer.getServer(), file1);
  }
}
