package net.minecraft.server;

import io.github.sasuked.severum.world.storage.PlayerDataStorage;
import io.github.sasuked.severum.world.storage.WorldDataStorage;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.UUID;

public class WorldNBTStorage implements IDataManager, IPlayerFileData {
    private static final boolean DISCARD_WORLD_DATA = Bukkit.getSeverumConfig().isDiscardWorldData();
    private static final boolean DISCARD_PLAYER_DATA = Bukkit.getSeverumConfig().isDiscardPlayerData();

    private final IDataManager worldDataStorage;
    private final IPlayerFileData playerDataStorage;

    public WorldNBTStorage(File file, String s, boolean flag) {
        this.worldDataStorage = new WorldDataStorage(new File(file, s));
        this.playerDataStorage = new PlayerDataStorage(this.worldDataStorage.getDirectory(), flag);
    }

    @Override
    public WorldData getWorldData() {
        return this.worldDataStorage.getWorldData();
    }

    @Override
    public void checkSession() throws ExceptionWorldConflict {
        this.worldDataStorage.checkSession();
    }

    @Override
    public IChunkLoader createChunkLoader(WorldProvider worldProvider) {
        return this.worldDataStorage.createChunkLoader(worldProvider);
    }

    @Override
    public void saveWorldData(WorldData worldData, NBTTagCompound compound) {
        this.worldDataStorage.saveWorldData(worldData, compound);
    }

    @Override
    public void saveWorldData(WorldData worldData) {
        this.worldDataStorage.saveWorldData(worldData);
    }

    public IPlayerFileData getPlayerFileData() {
        return this;
    }

    @Override
    public void a() {
        this.worldDataStorage.a();
    }

    @Override
    public File getDirectory() {
        return this.worldDataStorage.getDirectory();
    }

    public File getDataFile(String s) {
        return this.worldDataStorage.getDataFile(s);
    }

    @Override
    public String g() {
        return this.worldDataStorage.g();
    }

    @Override
    public UUID getUUID() {
        return this.worldDataStorage.getUUID();
    }

    @Override
    public void save(EntityHuman entityHuman) {
        this.playerDataStorage.save(entityHuman);
    }

    public NBTTagCompound load(EntityHuman entityHuman) {
        return this.playerDataStorage != null ? this.playerDataStorage.load(entityHuman) : null;
    }

    @Override
    public String[] getSeenPlayers() {
        return this.playerDataStorage.getSeenPlayers();
    }

    @Override
    public NBTTagCompound getPlayerData(String name) {
        return this.playerDataStorage.getPlayerData(name);
    }

    @Override
    public ServerStatisticManager createStatisticManager(EntityHuman entityHuman) {
        return this.playerDataStorage.createStatisticManager(entityHuman);
    }
}
