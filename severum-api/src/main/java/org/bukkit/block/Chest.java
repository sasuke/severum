package org.bukkit.block;

import org.bukkit.inventory.Inventory;

/**
 * Represents a chest.
 */
public interface Chest extends BlockState, ContainerBlock {

    /**
     * Returns the chest's inventory. If this is a double chest, it returns
     * just the portion of the inventory linked to this half of the chest.
     *
     * @return The inventory.
     */
    Inventory getBlockInventory();



    // Severum start

    /**
     * Changes the title of the chest inventory.
     *
     * @param newInventoryTitle
     */
    void setInventoryTitle(String newInventoryTitle);


    /**
     * Obtains the title of the chest inventory.
     *
     * @return current title of the chest inventory.
     */
    String getInventoryTitle();

    // Severum end
}
