package net.minecraft.server;

public interface IPlayerFileData {
    void save(EntityHuman entityhuman);
    NBTTagCompound load(EntityHuman entityhuman);

    String[] getSeenPlayers();

    NBTTagCompound getPlayerData(String name);

    ServerStatisticManager createStatisticManager(EntityHuman entityHuman);
}
