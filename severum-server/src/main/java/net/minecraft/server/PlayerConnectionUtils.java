package net.minecraft.server;

import io.github.sasuked.severum.ExploitException;

import java.util.HashMap;

public class PlayerConnectionUtils {

    public static <T extends PacketListener> void ensureMainThread(final Packet<T> packet,
                                                                   final T t0,
                                                                   IAsyncTaskHandler iasynctaskhandler)
      throws CancelledPacketHandleException {
        if (!iasynctaskhandler.isMainThread()) {
            iasynctaskhandler.postToMainThread(new Runnable() {
                public void run() {
                    try {
                        packet.a(t0);
                    } catch (ExploitException e) {
                        if (t0 instanceof PlayerConnection) {
                            MinecraftServer.getServer().server
                              .handleExploit(((PlayerConnection) t0).player, e, new HashMap<String, String>() {{
                                  put("packet", packet.getClass().getSimpleName());
                              }});
                        }
                    }
                }
            });
            throw CancelledPacketHandleException.INSTANCE;
        }
    }
}
