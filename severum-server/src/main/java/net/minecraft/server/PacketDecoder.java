package net.minecraft.server;

import io.github.sasuked.severum.ExploitException;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class PacketDecoder extends ByteToMessageDecoder {
    private static final Logger LOGGER = LogManager.getLogger();

    private static final Marker MARKER = MarkerManager.getMarker("PACKET_RECEIVED", NetworkManager.b);
    private final EnumProtocolDirection direction;
    private NetworkManager manager;

    public PacketDecoder(EnumProtocolDirection enumprotocoldirection) {
        this.direction = enumprotocoldirection;
    }

    public PacketDecoder(NetworkManager manager, EnumProtocolDirection direction) {
        this(direction);
        this.manager = manager;
    }

    protected void decode(ChannelHandlerContext channelhandlercontext,
                          ByteBuf bytebuf,
                          List<Object> list) throws Exception {
        if (bytebuf.readableBytes() != 0) {
            PacketDataSerializer packetdataserializer = new PacketDataSerializer(bytebuf);
            int i = packetdataserializer.e();
            Packet packet = channelhandlercontext.channel().attr(NetworkManager.c).get().a(this.direction, i);

            if (packet == null) {
                throw new IOException("Bad packet id " + i);
            } else {
                try {
                    if (bytebuf.readableBytes() > packet.maxLength()) {
                        throw new LargerPacketException(bytebuf.readableBytes(), packet.maxLength());
                    }

                    packet.a(packetdataserializer);
                } catch (ExploitException e) {
                    if (manager != null && manager.getPacketListener() instanceof PlayerConnection) {
                        MinecraftServer.getServer().server.handleExploit(
                          ((PlayerConnection) manager.getPacketListener()).player, e, new HashMap<String, String>() {{
                              put("packet", packet.getClass().getSimpleName());
                          }}
                        );
                    }
                }

                if (packetdataserializer.readableBytes() > 0) {
                    throw new IOException("Packet "
                                            + channelhandlercontext.channel().attr(NetworkManager.c).get().a() + "/" + i
                                            + " (" + packet.getClass()
                                                           .getSimpleName() + ") was larger than I expected, found "
                                            + packetdataserializer.readableBytes() + " bytes extra whilst reading packet " + i);
                } else {
                    list.add(packet);
                    if (PacketDecoder.LOGGER.isDebugEnabled()) {
                        PacketDecoder.LOGGER.debug(PacketDecoder.MARKER, " IN: [{}:{}] {}",
                                                   channelhandlercontext.channel().attr(NetworkManager.c).get(), i,
                                                   packet.getClass().getName());
                    }

                }
            }
        }
    }

    public static class LargerPacketException extends ExploitException {
        public LargerPacketException(int length, int max) {
            super(String.format("Buffer is larger than allowed (%d > %d)", length, max));
        }
    }
}
