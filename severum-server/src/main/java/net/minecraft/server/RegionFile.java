package net.minecraft.server;

import io.github.sasuked.severum.region.RegionCompressionType;
import com.github.luben.zstd.Zstd;
import com.github.luben.zstd.ZstdException;
import com.google.common.collect.Lists;
import org.github.paperspigot.exception.ServerInternalException;

import java.io.*;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.GZIPInputStream;
import net.jpountz.lz4.LZ4BlockOutputStream;
import org.github.paperspigot.PaperSpigotConfig;

public class RegionFile {

    private static final byte[] a = new byte[4096]; // Spigot - note: if this ever changes to not be 4096 bytes, update constructor! // PAIL: empty 4k block
    private final File b;
    private RandomAccessFile c;
    private final int[] d = new int[1024];
    private final int[] e = new int[1024];
    private List<Boolean> f;
    private int g;
    private long h;

    public RegionFile(File file) {
        this.b = file;
        this.g = 0;

        try {
            if (file.exists()) {
                this.h = file.lastModified();
            }

            this.c = new RandomAccessFile(file, "rw");
            int i;

            if (this.c.length() < 8192L) { // Severum - change 4096L to 8192L
                // Spigot - more effecient chunk zero'ing
                this.c.write(RegionFile.a); // Spigot
                this.c.write(RegionFile.a); // Spigot

                this.g += 8192;
            }

            if ((this.c.length() & 4095L) != 0L) {
                for (i = 0; (long) i < (this.c.length() & 4095L); ++i) {
                    this.c.write(0);
                }
            }

            i = (int) this.c.length() / 4096;
            this.f = Lists.newArrayListWithCapacity(i);

            int j;

            for (j = 0; j < i; ++j) {
                this.f.add(Boolean.valueOf(true));
            }

            this.f.set(0, Boolean.valueOf(false));
            this.f.set(1, Boolean.valueOf(false));
            this.c.seek(0L);

            int k;

            for (j = 0; j < 1024; ++j) {
                k = this.c.readInt();
                this.d[j] = k;
                if (k != 0 && (k >> 8) + (k & 255) <= this.f.size()) {
                    for (int l = 0; l < (k & 255); ++l) {
                        this.f.set((k >> 8) + l, Boolean.valueOf(false));
                    }
                }
            }

            for (j = 0; j < 1024; ++j) {
                k = this.c.readInt();
                this.e[j] = k;
            }
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
            ServerInternalException.reportInternalException(ioexception); // Paper
        }

    }

    // CraftBukkit start - This is a copy (sort of) of the method below it, make sure they stay in sync
    public synchronized boolean chunkExists(int i, int j) {
        if (this.d(i, j)) {
            return false;
        } else {
            try {
                int k = this.e(i, j);

                if (k == 0) {
                    return false;
                } else {
                    int l = k >> 8;
                    int i1 = k & 255;

                    if (l + i1 > this.f.size()) {
                        return false;
                    }

                    this.c.seek((long) (l * 4096));
                    int j1 = this.c.readInt();

                    if (j1 > 4096 * i1 || j1 <= 0) {
                        return false;
                    }

                    byte b0 = this.c.readByte();
                    if (b0 == 1 || b0 == 2) {
                        return true;
                    }
                }
            } catch (IOException ioexception) {
                return false;
            }
        }

        return false;
    }
    // CraftBukkit end

    // Severum - start
    private static final byte[] compressionBuffer =
            new byte[1024 * 64]; // 64k fits most standard chunks input size even, ideally 1 pass through zlib

    private static DirectByteArrayOutputStream compressData(byte[] buf, int length) throws IOException {
        // Severum start - new & optimized compression
        switch (PaperSpigotConfig.regionCompressionMethod) {
            case NONE: {
                DirectByteArrayOutputStream out = new DirectByteArrayOutputStream(length);
                out.write(buf, 0, length);
                out.close();
                return out;
            }
            case GZIP: {
                DirectByteArrayOutputStream out = new DirectByteArrayOutputStream(length);
                try (java.util.zip.GZIPOutputStream compressedOut = new java.util.zip.GZIPOutputStream(out, 32 * 1024)) {
                    compressedOut.write(buf);
                }
                out.close();
                return out;
            }
            case ZLIB: {
                DirectByteArrayOutputStream out = new DirectByteArrayOutputStream(length);
                synchronized (NBTCompressedStreamTools.zlibDeflater) {
                    NBTCompressedStreamTools.zlibDeflater.setInput(buf, 0, length);
                    NBTCompressedStreamTools.zlibDeflater.finish();
                    while (!NBTCompressedStreamTools.zlibDeflater.finished()) {
                        out.write(compressionBuffer, 0, NBTCompressedStreamTools.zlibDeflater.deflate(compressionBuffer));
                    }
                    out.close();
                    NBTCompressedStreamTools.zlibDeflater.reset();
                }
                return out;
            }
            case LZ4: {
                DirectByteArrayOutputStream out = new DirectByteArrayOutputStream(length);
                try (LZ4BlockOutputStream compressStream = new LZ4BlockOutputStream(out, 32 * 1024, NBTCompressedStreamTools.lz4Compressor)) {
                    compressStream.write(buf, 0, length);
                }
                out.close();
                return out;
            }
            case ZSTD: {
                byte[] output = new byte[(int) Zstd.compressBound(length)];
                DirectByteArrayOutputStream out = new DirectByteArrayOutputStream(output.length);
                long compressedBytes = Zstd.compressFastDict(output, 0, buf, 0, length, NBTCompressedStreamTools.zstdDictCompressor);
                if (Zstd.isError(compressedBytes)) {
                    throw new ZstdException(compressedBytes);
                }
                out.write(output, 0, (int) compressedBytes);
                out.close();
                return out;
            }
            default:
                return new DirectByteArrayOutputStream();
        }
        // Severum start - new & optimized compression
    }

    private static final byte[] decompressionBuffer =
            new byte[1024 * 64]; // 64k fits most standard chunks input size even, ideally 1 pass through zlib // Severum - new compression support

    // Severum - end

    public synchronized DataInputStream a(int i, int j) {
        if (this.d(i, j)) {
            return null;
        } else {
            try {
                int k = this.e(i, j);

                if (k == 0) {
                    return null;
                } else {
                    int l = k >> 8;
                    int i1 = k & 255;

                    if (l + i1 > this.f.size()) {
                        return null;
                    } else {
                        this.c.seek((long) (l * 4096));
                        int j1 = this.c.readInt();

                        if (j1 > 4096 * i1) {
                            return null;
                        } else if (j1 <= 0) {
                            return null;
                        } else {
                            byte b0 = this.c.readByte();
                            byte[] abyte;

                            // Severum - start
                            RegionCompressionType compressionType = RegionCompressionType.getFromType(b0);
                            if (compressionType == null) {
                                return null;
                            }

                            switch (compressionType) {
                                case NONE: {
                                    abyte = new byte[j1 - 1];
                                    this.c.read(abyte);
                                    return new DataInputStream(new BufferedInputStream(new ByteArrayInputStream(abyte), 32 * 1024));
                                }
                                case GZIP: {
                                    abyte = new byte[j1 - 1];
                                    this.c.read(abyte);
                                    return new DataInputStream(new BufferedInputStream(new GZIPInputStream(new ByteArrayInputStream(abyte), 32 * 1024), 32 * 1024));
                                }
                                case ZLIB: {
                                    abyte = new byte[j1 - 1];
                                    this.c.read(abyte);

                                    int power = 1;
                                    byte[] outBuf = new byte[1024 * 64 * power];
                                    int totalBytes = 0;
                                    synchronized (NBTCompressedStreamTools.zlibInflater) {
                                        NBTCompressedStreamTools.zlibInflater.setInput(abyte, 0, abyte.length);
                                        int decompressedBytes;
                                        while (!NBTCompressedStreamTools.zlibInflater.finished()) {
                                            boolean resize = false;
                                            decompressedBytes = NBTCompressedStreamTools.zlibInflater.inflate(decompressionBuffer);
                                            while (decompressedBytes > 1024 * 64 * power - totalBytes) {
                                                power++;
                                                resize = true;
                                            }
                                            if (resize) {
                                                byte[] tmp = outBuf;
                                                outBuf = new byte[1024 * 64 * power];
                                                System.arraycopy(tmp, 0, outBuf, 0, totalBytes);
                                            }
                                            System.arraycopy(decompressionBuffer, 0, outBuf, totalBytes, decompressedBytes);
                                            totalBytes += decompressedBytes;
                                        }
                                        NBTCompressedStreamTools.zlibInflater.reset();
                                    }
                                    abyte = new byte[totalBytes];
                                    System.arraycopy(outBuf, 0, abyte, 0, totalBytes);
                                    return new DataInputStream(new BufferedInputStream(new ByteArrayInputStream(abyte), 32 * 1024));
                                }
                                case LZ4: {
                                    abyte = new byte[j1 - 1];
                                    this.c.read(abyte);

                                    return new DataInputStream(new BufferedInputStream(
                                            new net.jpountz.lz4.LZ4BlockInputStream(
                                                    new ByteArrayInputStream(abyte), NBTCompressedStreamTools.lz4Decompressor), 32 * 1024));
                                }
                                case ZSTD: {
                                    abyte = new byte[j1 - 1];
                                    this.c.read(abyte);

                                    int power = 1;
                                    byte[] outBuf;
                                    long decompressedBytes;
                                    boolean resize;
                                    do {
                                        resize = false;
                                        outBuf = new byte[1024 * 64 * power];
                                        decompressedBytes = Zstd.decompressFastDict(outBuf, 0, abyte, 0, abyte.length, NBTCompressedStreamTools.zstdDictDecompressor);
                                        if (Zstd.isError(decompressedBytes)) {
                                            if (Zstd.getErrorCode(decompressedBytes) == Zstd.errDstSizeTooSmall()) {
                                                resize = true;
                                            } else {
                                                throw new ZstdException(decompressedBytes);
                                            }
                                        }

                                        if (resize) {
                                            power++;
                                        }
                                    } while (resize);
                                    abyte = new byte[(int) decompressedBytes];
                                    System.arraycopy(outBuf, 0, abyte, 0, (int) decompressedBytes);

                                    return new DataInputStream(new BufferedInputStream(new ByteArrayInputStream(abyte), 32 * 1024));
                                }
                                default:
                                    return null;
                            }
                            // Severum - end
                        }
                    }
                }
            } catch (IOException | DataFormatException | ZstdException ioexception) {
                return null;
            }
        }
    }

    public DataOutputStream b(int i, int j) throws IOException { // PAIL: getChunkOutputStream
        return this.d(i, j) ?
                null : new DataOutputStream(new RegionFile.ChunkBuffer(i, j)); // Spigot - use a BufferedOutputStream to greatly improve file write performance
    }

    protected synchronized void a(int i, int j, byte[] abyte, int count) {
        try {
            int l = this.e(i, j);
            int i1 = l >> 8;
            int j1 = l & 255;
            int k1 = (count + 5) / 4096 + 1;

            if (k1 >= 256) {
                return;
            }

            if (i1 != 0 && j1 == k1) {
                this.a(i1, abyte, count);
            } else {
                int l1;

                for (l1 = 0; l1 < j1; ++l1) {
                    this.f.set(i1 + l1, Boolean.valueOf(true));
                }

                l1 = this.f.indexOf(Boolean.valueOf(true));
                int i2 = 0;
                int j2;

                if (l1 != -1) {
                    for (j2 = l1; j2 < this.f.size(); ++j2) {
                        if (i2 != 0) {
                            if (((Boolean) this.f.get(j2)).booleanValue()) {
                                ++i2;
                            } else {
                                i2 = 0;
                            }
                        } else if (((Boolean) this.f.get(j2)).booleanValue()) {
                            l1 = j2;
                            i2 = 1;
                        }

                        if (i2 >= k1) {
                            break;
                        }
                    }
                }

                if (i2 >= k1) {
                    i1 = l1;
                    this.a(i, j, l1 << 8 | k1);

                    for (j2 = 0; j2 < k1; ++j2) {
                        this.f.set(i1 + j2, Boolean.valueOf(false));
                    }

                    this.a(i1, abyte, count);
                } else {
                    this.c.seek(this.c.length());
                    i1 = this.f.size();

                    for (j2 = 0; j2 < k1; ++j2) {
                        this.c.write(RegionFile.a);
                        this.f.add(Boolean.valueOf(false));
                    }

                    this.g += 4096 * k1;
                    this.a(i1, abyte, count);
                    this.a(i, j, i1 << 8 | k1);
                }
            }

            this.b(i, j, (int) (MinecraftServer.az() / 1000L));
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
            ServerInternalException.reportInternalException(ioexception); // Paper
        }
    }

    private void a(int i, byte[] abyte, int j) throws IOException {
        this.c.seek((long) (i * 4096));
        this.c.writeInt(j + 1);
        this.c.writeByte((byte) PaperSpigotConfig.regionCompressionMethod.getType()); // Severum - add compression support
        this.c.write(abyte, 0, j);
    }

    private boolean d(int i, int j) {
        return i < 0 || i >= 32 || j < 0 || j >= 32;
    }

    private int e(int i, int j) {
        return this.d[i + j * 32];
    }

    public boolean c(int i, int j) {
        return this.e(i, j) != 0;
    }

    private void a(int i, int j, int k) throws IOException {
        this.d[i + j * 32] = k;
        this.c.seek((long) ((i + j * 32) * 4));
        this.c.writeInt(k);
    }

    private void b(int i, int j, int k) throws IOException {
        this.e[i + j * 32] = k;
        this.c.seek((long) (4096 + (i + j * 32) * 4));
        this.c.writeInt(k);
    }

    public void c() throws IOException {
        if (this.c != null) {
            this.c.close();
        }

    }

    // Severum - start
    private static class DirectByteArrayOutputStream extends ByteArrayOutputStream {

        public DirectByteArrayOutputStream() {
            super();
        }

        public DirectByteArrayOutputStream(int size) {
            super(size);
        }

        public byte[] getBuffer() {
            return this.buf;
        }

    }
    // Severum - end

    class ChunkBuffer extends ByteArrayOutputStream {

        private int b;
        private int c;

        public ChunkBuffer(int i, int j) {
            super(8096);
            this.b = i;
            this.c = j;
        }

        public void close() throws IOException {
            // Severum - start
            int origLength = this.count;
            byte[] buf = this.buf;

            DirectByteArrayOutputStream out = compressData(buf, origLength);
            byte[] bytes = out.getBuffer();
            int length = out.size();
            // Severum - end

            RegionFile.this.a(this.b, this.c, bytes, length); // Severum - change to bytes/length
        }
    }

}
