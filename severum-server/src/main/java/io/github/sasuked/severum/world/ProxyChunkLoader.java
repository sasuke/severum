package io.github.sasuked.severum.world;

import net.minecraft.server.*;
import org.bukkit.Bukkit;

import java.io.File;

/**
 * @author Luiz Carlos Mourão
 */
public class ProxyChunkLoader extends ChunkRegionLoader implements IChunkLoader, IAsyncChunkSaver {
    private static final boolean DISCARD_WORLD_DATA = Bukkit.getSeverumConfig().isDiscardWorldData();

    public ProxyChunkLoader(File file) {
        super(file);
    }

    @Override
    public boolean c() {
        return !DISCARD_WORLD_DATA && super.c();
    }

    @Override
    public void b() {
        if (DISCARD_WORLD_DATA) return;
        super.b();
    }
}
