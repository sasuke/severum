package io.github.sasuked.severum.api;

import org.bukkit.entity.Player;

import java.util.Map;
import java.util.function.Consumer;

/**
 * @author Matheus Barreto
 */


public class SeverumConfig {
  private boolean discardWorldData;
  private boolean discardPlayerData;
  private boolean booksDisabled;
  private boolean hideCommands;
  private String serverBrand = "Severum";

  private boolean entityAiEnabled = true;
  private boolean naturalBlockUpdatingEnabled = true;

  private boolean spawnerParticlesEnabled = false;

  public boolean isDiscardWorldData() {
    return discardWorldData;
  }

  public boolean isDiscardPlayerData() {
    return discardPlayerData;
  }

  public boolean areBooksDisabled() {
    return booksDisabled;
  }

  public boolean isHideCommands() {
    return hideCommands;
  }

  public String getServerBrand() {
    return serverBrand;
  }

  public void setDiscardWorldData(boolean discardWorldData) {
    this.discardWorldData = discardWorldData;
  }

  public void setDiscardPlayerData(boolean discardPlayerData) {
    this.discardPlayerData = discardPlayerData;
  }

  public void setBooksDisabled(boolean booksDisabled) {
    this.booksDisabled = booksDisabled;
  }

  public void setHideCommands(boolean hideCommands) {
    this.hideCommands = hideCommands;
  }

  public void setServerBrand(String serverBrand) {
    this.serverBrand = serverBrand;
  }

  public boolean isEntityAiEnabled() {
    return entityAiEnabled;
  }

  public void setEntityAiEnabled(boolean entityAiEnabled) {
    this.entityAiEnabled = entityAiEnabled;
  }

  public boolean isNaturalBlockUpdateEnabled() {
    return naturalBlockUpdatingEnabled;
  }

  public void setNaturalBlockUpdatingEnabled(boolean naturalBlockUpdatingEnabled) {
    this.naturalBlockUpdatingEnabled = naturalBlockUpdatingEnabled;
  }

  public boolean isSpawnerParticlesEnabled() {
    return spawnerParticlesEnabled;
  }

  public void setSpawnerParticlesEnabled(boolean spawnerParticlesEnabled) {
    this.spawnerParticlesEnabled = spawnerParticlesEnabled;
  }

  // Exploit handling

  private Consumer<ExploitContext> exploitHook;

  public Consumer<ExploitContext> getExploitHook() {
    return exploitHook;
  }

  public void setExploitHook(Consumer<ExploitContext> exploitHook) {
    this.exploitHook = exploitHook;
  }

  public static class ExploitContext {
    private Player player;
    private String kickReason = "Aborted";
    private Map<String, String> data;

    public ExploitContext(Player player, Map<String, String> data) {
      this.player = player;
      this.data = data;
    }

    public Player getPlayer() {
      return player;
    }

    public String getKickReason() {
      return kickReason;
    }

    public Map<String, String> getData() {
      return data;
    }

    public void setKickReason(String kickReason) {
      this.kickReason = kickReason;
    }
  }
}
