package org.bukkit.event.entity;

import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PreSpawnerSpawnEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private final CreatureSpawner spawner;
    private final EntityType entityType;
    private final String entityTypeName;
    private boolean canceled;

    public PreSpawnerSpawnEvent(final CreatureSpawner spawner, EntityType entityType, String entityTypeName) {
        this.spawner = spawner;
        this.entityType = entityType;
        this.entityTypeName = entityTypeName;
    }

    public CreatureSpawner getSpawner() {
        return spawner;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public String getEntityTypeName() {
        return entityTypeName;
    }

    public boolean isCancelled() {
        return canceled;
    }

    public void setCancelled(boolean cancel) {
        canceled = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
