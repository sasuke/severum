package org.bukkit.inventory.meta;

import org.bukkit.Material;

/**
 * Represents a skull ({@link Material#SKULL_ITEM}) that can have an owner.
 */
public interface SkullMeta extends ItemMeta {

    /**
     * Gets the owner of the skull.
     *
     * @return the owner if the skull
     */
    String getOwner();

    /**
     * Checks to see if the skull has an owner.
     *
     * @return true if the skull has an owner
     */
    boolean hasOwner();

    /**
     * Sets the owner of the skull.
     * <p>
     * Plugins should check that hasOwner() returns true before calling this
     * plugin.
     *
     * @param owner the new owner of the skull
     * @return true if the owner was successfully set
     */
    boolean setOwner(String owner);

    /**
     * Forces a custom skin texture from given base64 hash.
     *
     * @param base64 skin texture base64
     * @return true if the texture was successfully set
     */
    boolean setTexture(String base64);


    SkullMeta clone();
}
