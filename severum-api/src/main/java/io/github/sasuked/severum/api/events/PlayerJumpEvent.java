package io.github.sasuked.severum.api.events;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Ryan Lopes
 */
public class PlayerJumpEvent extends Event implements Cancellable {
    
    private static final HandlerList handlers = new HandlerList();
    private final Player player;
    private final Location fromLocation, toLocation;
    private boolean cancel = false;
    
    public PlayerJumpEvent(final Player player, final Location fromLocation, final Location toLocation) {
        this.player = player;
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public Location getFromLocation() {
        return fromLocation;
    }
    
    public Location getToLocation() {
        return toLocation;
    }
    
    /**
     * Gets the cancellation state of this event. A cancelled event will not
     * be executed in the server, but will still pass to other plugins
     *
     * @return true if this event is cancelled
     */
    public boolean isCancelled() {
        return cancel;
    }

    /**
     * Sets the cancellation state of this event. A cancelled event will not
     * be executed in the server, but will still pass to other plugins
     *
     * @param cancel true if you wish to cancel this event
     */
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}
