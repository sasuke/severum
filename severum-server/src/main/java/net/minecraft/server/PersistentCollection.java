package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class PersistentCollection {

    private IDataManager b;
    protected Map<String, PersistentBase> a = Maps.newHashMap();
    public List<PersistentBase> c = Lists.newArrayList(); // Spigot
    private Map<String, Short> d = Maps.newHashMap();

    public PersistentCollection(IDataManager idatamanager) {
        this.b = idatamanager;
        this.b();
    }

    public PersistentBase get(Class<? extends PersistentBase> oclass, String s) {
        return this.a.get(s);
    }

    public void a(String s, PersistentBase persistentbase) {
        if (this.a.containsKey(s)) {
            this.c.remove(this.a.remove(s));
        }

        this.a.put(s, persistentbase);
        this.c.add(persistentbase);
    }

    public void a() {
        for (int i = 0; i < this.c.size(); ++i) {
            PersistentBase persistentbase = this.c.get(i);

            if (persistentbase.d()) {
                this.a(persistentbase);
                persistentbase.a(false);
            }
        }

    }

    private void a(PersistentBase persistentbase) {
    }

    private void b() {
        this.d.clear();
    }

    public int a(String s) {
        Short oshort = this.d.get(s);

        if (oshort == null) {
            oshort = (short) 0;
        } else {
            oshort = (short) (oshort + 1);
        }

        this.d.put(s, oshort);
        return oshort;
    }
}
