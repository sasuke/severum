package net.minecraft.server;

import io.github.sasuked.severum.ExploitException;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class SystemUtils {

    public static <V> V a(FutureTask<V> futuretask, Logger logger) {
        try {
            futuretask.run();
            return futuretask.get();
        } catch (ExecutionException e) {
            if ((!(e.getCause() instanceof ExploitException))) {
                logger.fatal("Error executing task", e);
            }
        } catch (InterruptedException interruptedexception) {
            logger.fatal("Error executing task", interruptedexception);
        }

        return null;
    }
}
