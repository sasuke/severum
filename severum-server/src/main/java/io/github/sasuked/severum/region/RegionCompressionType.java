package io.github.sasuked.severum.region;

import java.util.stream.Stream;

/**
 *
 * @author Ryan Lopes
 */
public enum RegionCompressionType {
    
    NONE(0, "none"),
    GZIP(1, "gzip"),
    ZLIB(2, "zlib"),
    LZ4(126, "lz4"),
    ZSTD(127, "zstd");
    
    private final int type;
    private final String name;
    
    RegionCompressionType(int type, String name) {
        this.type = type;
        this.name = name;
    }
    
    public int getType() { 
        return type;
    }
    
    public String getName() {
        return name;
    }
    
    public static RegionCompressionType getFromName(String name) {
        return Stream.of(values()).filter(value -> value.getName().equals(name)).findFirst().orElse(null);
    }
    
    public static RegionCompressionType getFromType(int type) {
        return Stream.of(values()).filter(value -> value.getType() == type).findFirst().orElse(null);
    }
    
    @Override
    public String toString() { 
        return name;
    }
    
}
