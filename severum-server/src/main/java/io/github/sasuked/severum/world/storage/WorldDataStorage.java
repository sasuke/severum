package io.github.sasuked.severum.world.storage;

import net.minecraft.server.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bukkit.Bukkit;
import org.github.paperspigot.exception.ServerInternalException;

import java.io.*;
import java.nio.file.Files;
import java.util.UUID;

/**
 * @author Luiz Carlos Mourão
 */
public class WorldDataStorage implements IDataManager {
    private static final boolean DISCARD_WORLD_DATA = Bukkit.getSeverumConfig().isDiscardWorldData();

    private static final Logger LOGGER = LogManager.getLogger();

    private final File baseDir;
    private final File dataDir;
    private final long sessionId = MinecraftServer.az();

    private UUID uuid;

    public WorldDataStorage(File baseDir) {
        this.baseDir = baseDir;

        if (DISCARD_WORLD_DATA) {
            this.dataDir = null;
            this.uuid = UUID.randomUUID();
            return;
        }

        this.dataDir = new File(baseDir, "data");
        this.dataDir.mkdirs();

        updateSession();
    }

    private void updateSession() {
        if (DISCARD_WORLD_DATA) return;

        File file = new File(this.baseDir, "session.lock");
        try (DataOutputStream stream = new DataOutputStream(new FileOutputStream(file))) {
            stream.writeLong(this.sessionId);
        } catch (IOException e) {
            throw new RuntimeException("failed to lock session in " + this.baseDir); // Spigot
        }
    }

    @Override
    public WorldData getWorldData() {
        File file = new File(this.baseDir, "level.dat");

        if (file.exists()) {
            try {
                return new WorldData(NBTCompressedStreamTools.a(new FileInputStream(file)).getCompound("Data"));
            } catch (Exception e) {
                LOGGER.error("failed to load world data", e);
                ServerInternalException.reportInternalException(e); // Paper
            }
        }

        return null;
    }

    @Override
    public void checkSession() throws ExceptionWorldConflict {
        if (DISCARD_WORLD_DATA) return;

        File file = new File(this.baseDir, "session.lock");
        try (DataInputStream stream = new DataInputStream(new FileInputStream(file))) {
            if (stream.readLong() != this.sessionId) {
                throw new ExceptionWorldConflict(this.baseDir + " is already locked by another process");  // Spigot
            }
        } catch (IOException e) {
            throw new ExceptionWorldConflict("failed to lock session in " + this.baseDir); // Spigot
        }
    }

    @Override
    public IChunkLoader createChunkLoader(WorldProvider worldprovider) {
        throw new RuntimeException("Old Chunk Storage is no longer supported.");
    }

    @Override
    public void saveWorldData(WorldData worldData, NBTTagCompound compound) {
        if (DISCARD_WORLD_DATA) return;

        NBTTagCompound worldCompound = worldData.a(compound);
        NBTTagCompound dataCompound = new NBTTagCompound();

        dataCompound.set("Data", worldCompound);

        try {
            NBTCompressedStreamTools
              .a(dataCompound, Files.newOutputStream(this.baseDir.toPath().resolve("level.dat")));
        } catch (Exception e) {
            LOGGER.warn("failed to save world data", e);
            ServerInternalException.reportInternalException(e); // Paper
        }
    }

    @Override
    public void saveWorldData(WorldData worldData) {
        if (DISCARD_WORLD_DATA) return;

        NBTTagCompound worldCompound = worldData.a();
        NBTTagCompound dataCompound = new NBTTagCompound();

        dataCompound.set("Data", worldCompound);

        try {
            NBTCompressedStreamTools
              .a(dataCompound, Files.newOutputStream(this.baseDir.toPath().resolve("level.dat")));
        } catch (Exception e) {
            LOGGER.warn("failed to save world data", e);
            ServerInternalException.reportInternalException(e); // Paper
        }
    }

    @Override
    public IPlayerFileData getPlayerFileData() {
        return null;
    }

    @Override
    public void a() {

    }

    @Override
    public File getDirectory() {
        return this.baseDir;
    }

    @Override
    public File getDataFile(String s) {
        return new File(this.dataDir, s + ".dat");
    }

    @Override
    public String g() {
        return this.baseDir.getName();
    }

    @Override
    public UUID getUUID() {
        if (uuid != null) return uuid;

        File uuidFile = new File(this.baseDir, "uid.dat");
        if (uuidFile.exists()) {
            try (DataInputStream stream = new DataInputStream(new FileInputStream(uuidFile))) {
                return uuid = new UUID(stream.readLong(), stream.readLong());
            } catch (IOException e) {
                LOGGER.warn("failed to read {}, generating new random UUID", uuidFile, e);
            }
        }

        uuid = UUID.randomUUID();
        if (!DISCARD_WORLD_DATA) {
            try (DataOutputStream stream = new DataOutputStream(new FileOutputStream(uuidFile))) {
                stream.writeLong(uuid.getMostSignificantBits());
                stream.writeLong(uuid.getLeastSignificantBits());
            } catch (IOException e) {
                LOGGER.warn("failed to write uuid to {}", uuidFile, e);
            }
        }

        return uuid;
    }
}
