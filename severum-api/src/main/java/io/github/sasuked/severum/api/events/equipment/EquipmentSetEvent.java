package io.github.sasuked.severum.api.events.equipment;

import org.bukkit.entity.HumanEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

/**
 * Called after a human entity changes equipment.
 *
 * @author Matheus Barreto
 */
public class EquipmentSetEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private final HumanEntity humanEntity;
    private final EquipmentSlot slot;
    private final ItemStack previousItem;
    private final ItemStack newItem;

    private boolean cancelled;

    public EquipmentSetEvent(HumanEntity humanEntity, EquipmentSlot slot, ItemStack previousItem, ItemStack newItem) {
        this.humanEntity = humanEntity;
        this.slot = slot;
        this.previousItem = previousItem;
        this.newItem = newItem;
    }

    public HumanEntity getHumanEntity() {
        return this.humanEntity;
    }

    public EquipmentSlot getSlot() {
        return this.slot;
    }

    public ItemStack getPreviousItem() {
        return this.previousItem;
    }

    public ItemStack getNewItem() {
        return this.newItem;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
