package net.minecraft.server;

import io.github.sasuked.severum.ExploitException;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.CorruptedFrameException;

import java.util.HashMap;
import java.util.List;

public class PacketSplitter extends ByteToMessageDecoder {
    private static final int MAX_PACKET_THRESHOLD = Short.MAX_VALUE;

    private int currentWarning = 0;
    private final NetworkManager manager;

    public PacketSplitter() {
        this(null);
    }

    public PacketSplitter(NetworkManager manager) {
        this.manager = manager;
    }

    protected void decode(ChannelHandlerContext channelhandlercontext, ByteBuf bytebuf, List<Object> list) throws Exception {
        bytebuf.markReaderIndex();
        byte[] abyte = new byte[3];

        for (int i = 0; i < abyte.length; ++i) {
            if (!bytebuf.isReadable()) {
                bytebuf.resetReaderIndex();
                return;
            }

            abyte[i] = bytebuf.readByte();
            if (abyte[i] >= 0) {
                PacketDataSerializer packetdataserializer = new PacketDataSerializer(Unpooled.wrappedBuffer(abyte));

                try {
                    int j = packetdataserializer.e();

                    // Severum - start
                    if (j > MAX_PACKET_THRESHOLD) {
                        throw new ExploitException("packet over maximum allowed");
                    }
                    // Severum - end

                    if (bytebuf.readableBytes() >= j) {
                        list.add(bytebuf.readBytes(j));
                        return;
                    }

                    bytebuf.resetReaderIndex();
                } finally {
                    packetdataserializer.release();
                }

                return;
            }
        }

        throw new CorruptedFrameException("length wider than 21-bit");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable throwable) throws Exception {
        if (manager != null
          && manager.getPacketListener() instanceof PlayerConnection
          && throwable instanceof ExploitException) {
            MinecraftServer.getServer().server
              .handleExploit(((PlayerConnection) manager.getPacketListener()).player,
                             ((ExploitException) throwable),
                             new HashMap<>());
        }

        super.exceptionCaught(channelHandlerContext, throwable);
    }
}
